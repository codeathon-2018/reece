import RPi.GPIO as GPIO
from time  import sleep
servono = [03,05,07,11,12,13]
def SetAngle(angle,servo):
    if angle >= 0 and angle <= 180:
        servoChoice = servono[servo-1]
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(servoChoice,GPIO.OUT)
        pwm=GPIO.PWM(servoChoice,50)    
        pwm.start(0)
        duty = (angle/22.5)+3.5
        GPIO.output(servoChoice,True)
        pwm.ChangeDutyCycle(duty)
        sleep(0.275)
        pwm.ChangeDutyCycle(7.5)
        sleep(0.275)
        GPIO.output(servoChoice,False)
        pwm.ChangeDutyCycle(0)
        pwm.stop()
        GPIO.cleanup()
    else:
        print("Angle out of bounds")
        
SetAngle(input("Enter angle: "), input("Enter Servo: "))




